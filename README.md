# Kalman_filter_IMU_sensor

This is a project made for SZDOS - Sistemi Za Digitalnu Obradu Signala (eng. Systems For Digital Signal Processing) college class
It uses TM4C123GH6PM microcontroler and a MPU6050 6DOF sensor to calculate yaw and pitch angle.
Measurements are filtered with Kalman filter.
Main focus of this project is Kalman filter implementation.
Results can found in a document "Implementacija_Kalmanovog_filtra_za_rad_u_realnom_vremenu" (written in Serbian language)
