/*
 * UART_specific.c
 *
 *  Created on: Jan 29, 2020
 *      Author: damja
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"

#include "utils/uartstdio.h"
#include "driverlib/rom.h"

#include "inc/hw_i2c.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "driverlib/i2c.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"

#include "inc/hw_ints.h"
#include "driverlib/interrupt.h"

#include "UART_specific.h"

void ConfigureUART(void)
{
    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);

    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);

    ROM_GPIOPinConfigure(GPIO_PA0_U0RX);
    ROM_GPIOPinConfigure(GPIO_PA1_U0TX);
    ROM_GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);

    UARTClockSourceSet(UART0_BASE, UART_CLOCK_PIOSC);

    UARTStdioConfig(0, 115200, 16000000);
}

void printEverything(float AccX, float AccY, float AccZ, float GyroX, float GyroY, float GyroZ, float filtered_angleX, float raw_angleX, float filtered_angleY, float raw_angleY)
{
    char data_to_str[40];

    sprintf(data_to_str,"%06.3f#%06.3f#%06.3f#", AccX, AccY, AccZ);
    UARTprintf(data_to_str);
    sprintf(data_to_str,"%06.3f#%06.3f#%06.3f#", GyroX, GyroY, GyroZ);
    UARTprintf(data_to_str);
    sprintf(data_to_str,"%06.3f#%06.3f#", filtered_angleX, raw_angleX);
    UARTprintf(data_to_str);
    sprintf(data_to_str,"%06.3f#%06.3f\n", filtered_angleY, raw_angleY);
    UARTprintf(data_to_str);
}

void printAngles(float filtered_angleX, float raw_angleX, float filtered_angleY, float raw_angleY)
{
    char data_to_str[40];

    sprintf(data_to_str,"%06.3f#%06.3f#", filtered_angleX, raw_angleX);
    UARTprintf(data_to_str);
    sprintf(data_to_str,"%06.3f#%06.3f\n", filtered_angleY, raw_angleY);
    UARTprintf(data_to_str);
    return;
}

void printValues(float AccX, float AccY, float AccZ, float GyroX, float GyroY, float GyroZ)
{
    char data_to_str[40];

    sprintf(data_to_str,"%06.3f#%06.3f#%06.3f#", AccX, AccY, AccZ);
    UARTprintf(data_to_str);
    sprintf(data_to_str,"%06.3f#%06.3f#%06.3f\n", GyroX, GyroY, GyroZ);
    UARTprintf(data_to_str);
}
