/*
 * UART_specific.h
 *
 *  Created on: Dec 31, 2019
 *      Author: damja
 */

#ifndef UART_SPECIFIC_H_
#define UART_SPECIFIC_H_

/**
 * @brief Initialization of UART module
 * 
 */
void ConfigureUART(void);

/**
 * @brief Prints raw sensor values via UART, padded with %06.3f and '#' as a separation character
 * 
 * @param AccX Raw acceleration x value 
 * @param AccY Raw acceleration y value 
 * @param AccZ Raw acceleration z value 
 * @param GyroX Raw gyroscope x value 
 * @param GyroY Raw gyroscope y value 
 * @param GyroZ Raw gyroscope z value 
 */
void printValues(float AccX, float AccY, float AccZ, float GyroX, float GyroY, float GyroZ);

/**
 * @brief Prints raw sensor values, angle values and filtered angle values via UART, padded with %06.3f and '#' as a separation character
 * 
 * @param AccX Raw acceleration x value 
 * @param AccY Raw acceleration y value 
 * @param AccZ Raw acceleration z value 
 * @param GyroX Raw gyroscope x value 
 * @param GyroY Raw gyroscope y value 
 * @param GyroZ Raw gyroscope z value 
 * @param filtered_angleX Kalman filtered yaw angle
 * @param raw_angleX Raw yaw angle
 * @param filtered_angleY Kalman filtered pitch angle
 * @param raw_angleY Raw pitch angle
 */
void printEverything(float AccX, float AccY, float AccZ, float GyroX, float GyroY, float GyroZ, float filtered_angleX, float raw_angleX, float filtered_angleY, float raw_angleY);

/**
 * @brief Prints raw angle values and filtered angle values via UART, padded with %06.3f and '#' as a separation character
 * 
 * @param filtered_angleX Kalman filtered yaw angle
 * @param raw_angleX Raw yaw angle
 * @param filtered_angleY Kalman filtered pitch angle
 * @param raw_angleY Raw pitch angle
 */
void printAngles(float filtered_angleX, float raw_angleX, float filtered_angleY, float raw_angleY);

#endif /* UART_SPECIFIC_H_ */
