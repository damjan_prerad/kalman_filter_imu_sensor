/*
 * kalman_calculations.c
 *
 *  Created on: Jan 29, 2020
 *      Author: damja
 */

#include "kalman_calculations.h"

void applyKalmanFiltering(float zk, float omega_current, float X[2], float P[2][2])
{
    float X_priori[2] = { 0.0f };

    /*
     * A priori estimate calculation
     */
    X_priori[1] = omega_current - X[1];
    X_priori[0] = X[0] + dt * X_priori[1];

    /*
     * A priori error covariance matrix calculation
     */
    float P_priori[2][2];
    P_priori[0][0] = P[0][0] + dt * (dt * P[1][1] - P[0][1] - P[1][0] + Q_theta);
    P_priori[0][1] = P[0][1] - dt * P[1][1];
    P_priori[1][0] = P[1][0] - dt * P[1][1];
    P_priori[1][1] = P[1][1] + Q_omega * dt;

    /*
     * Inovation
     */
    float y = zk - X_priori[0];

    /*
     * Inovation matrix
     */
    float S = P_priori[0][0] + R;

    /*
     * Kalman gain calculation
     */
    float K_gain[2] = { 0 };
    K_gain[0] = P_priori[0][0] / S;
    K_gain[1] = P_priori[1][0] / S;

    /*
     * A posteriori estimate calculation
     */
    X[0] = X_priori[0] + K_gain[0] * y;
    X[1] += K_gain[1] * y;

    /*
     * A posteriori error covariance eror calculation
     */
    P[0][0] = P_priori[0][0]  - K_gain[0] * P_priori[0][0];
    P[0][1] = P_priori[0][1]  - K_gain[0] * P_priori[0][1];
    P[1][0] = P_priori[1][0]  - K_gain[1] * P_priori[0][0];
    P[1][1] = P_priori[1][1]  - K_gain[1] * P_priori[0][1];

}

