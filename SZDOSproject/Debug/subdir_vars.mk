################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../tm4c123gh6pm.cmd 

C_SRCS += \
../MPU6050_control.c \
../UART_specific.c \
../i2c_functions.c \
../i2cm_drv.c \
../kalman_calculations.c \
../main.c \
../mpu6050.c \
../tm4c123gh6pm_startup_ccs.c \
../uartstdio.c 

C_DEPS += \
./MPU6050_control.d \
./UART_specific.d \
./i2c_functions.d \
./i2cm_drv.d \
./kalman_calculations.d \
./main.d \
./mpu6050.d \
./tm4c123gh6pm_startup_ccs.d \
./uartstdio.d 

OBJS += \
./MPU6050_control.obj \
./UART_specific.obj \
./i2c_functions.obj \
./i2cm_drv.obj \
./kalman_calculations.obj \
./main.obj \
./mpu6050.obj \
./tm4c123gh6pm_startup_ccs.obj \
./uartstdio.obj 

OBJS__QUOTED += \
"MPU6050_control.obj" \
"UART_specific.obj" \
"i2c_functions.obj" \
"i2cm_drv.obj" \
"kalman_calculations.obj" \
"main.obj" \
"mpu6050.obj" \
"tm4c123gh6pm_startup_ccs.obj" \
"uartstdio.obj" 

C_DEPS__QUOTED += \
"MPU6050_control.d" \
"UART_specific.d" \
"i2c_functions.d" \
"i2cm_drv.d" \
"kalman_calculations.d" \
"main.d" \
"mpu6050.d" \
"tm4c123gh6pm_startup_ccs.d" \
"uartstdio.d" 

C_SRCS__QUOTED += \
"../MPU6050_control.c" \
"../UART_specific.c" \
"../i2c_functions.c" \
"../i2cm_drv.c" \
"../kalman_calculations.c" \
"../main.c" \
"../mpu6050.c" \
"../tm4c123gh6pm_startup_ccs.c" \
"../uartstdio.c" 


