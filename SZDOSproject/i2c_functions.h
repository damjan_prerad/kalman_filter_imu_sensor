/*
 * i2c_functions.h
 *
 *  Created on: Dec 31, 2019
 *      Author: damja
 */

#ifndef I2C_FUNCTIONS_H_
#define I2C_FUNCTIONS_H_

/**
 * @brief I2C0 initialization, uses pins PB2 and PB3
 * 
 */
void InitI2C0(void);

/**
 * @brief Sends num_of_args bytes via I2C bus
 * 
 * @param slave_addr Slave device address
 * @param num_of_args Number of bytes to send
 * @param ... data to send
 */
void I2CSend(uint8_t slave_addr, uint8_t num_of_args, ...);

/**
 * @brief Receive one byte from I2C slave device
 * 
 * @param slave_addr Slave device address
 * @param reg Register to read
 * @return uint8_t register value
 */
uint8_t I2CReceive(uint32_t slave_addr, uint8_t reg);

/**
 * @brief Receives 6 bytes form I2C slave device (optimized for MPU6050 to read accel data and gyro data)
 * 
 * @param slave_addr Slave device address
 * @param reg Start register
 * @param data Array to store register values
 */
void I2CReceive6Bytes(uint32_t slave_addr, uint8_t reg, int16_t* data);

#endif /* I2C_FUNCTIONS_H_ */
