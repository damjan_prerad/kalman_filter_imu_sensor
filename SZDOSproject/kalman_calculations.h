/*
 * kalman_calculations.h
 *
 *  Created on: Dec 31, 2019
 *      Author: damja
 */

#ifndef KALMAN_CALCULATIONS_H_
#define KALMAN_CALCULATIONS_H_

/**
 * @brief Accelerometer process noise
 * 
 */
extern float Q_theta;

/**
 * @brief Gyroscope process noise
 * 
 */
extern float Q_omega;

/**
 * @brief Measurement noise
 * 
 */
extern float R;

/**
 * @brief Iterration period
 * 
 */
extern float dt;

/**
 * @brief Function to perform Kalman filtering
 * 
 * @param zk Measured angle
 * @param omega_current Current angular velocity
 * @param X State vector
 * @param P Previous error covariance matrix
 */
void applyKalmanFiltering(float zk, float omega_current, float X[2], float P[2][2]);

#endif /* KALMAN_CALCULATIONS_H_ */
