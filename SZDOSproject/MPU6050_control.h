/*
 * MPU6050_control.h
 *
 *  Created on: Dec 31, 2019
 *      Author: damja
 */

#ifndef MPU6050_CONTROL_H_
#define MPU6050_CONTROL_H_

#define MPU6050_I2C_ADDRESS 0x68

#define PWR_MGMT_1 0x6B

#define SMPRT_DIV 0x19

#define INT_PIN_CFG 0x37 //0b11010000 = 0xD0
#define INT_ENABLE 0x38 //0b00000001 = 0x01
#define INT_STATUS 0X3A

#define CONFIG 0x1A
#define GYRO_CONFIG 0x1B
#define ACCEL_CONFIG 0x1C

#define WHO_AM_I 0x75 // returns 68

#define ACCEL_XOUT_H 0x3B
#define ACCEL_XOUT_L 0x3C
#define ACCEL_YOUT_H 0x3D
#define ACCEL_YOUT_L 0x3E
#define ACCEL_ZOUT_H 0x3F
#define ACCEL_ZOUT_L 0x40

#define GYRO_XOUT_H 0x43
#define GYRO_XOUT_L 0x44
#define GYRO_YOUT_H 0x45
#define GYRO_YOUT_L 0x46
#define GYRO_ZOUT_H 0x47
#define GYRO_ZOUT_L 0x48

extern float AccX;
extern float AccY;
extern float AccZ;

extern float GyroX;
extern float GyroY;
extern float GyroZ;

/**
 * @brief MPU6050 sensor initialization
 * 
 * @return uint8_t 0 for succes, 1 for error
 */
uint8_t initMPU6050(void);

/**
 * @brief Get accelerometer and gyroscope values from MPU6050
 * 
 */
void getValues(void);

/**
 * @brief Convert raw data to physical values
 * 
 */
void convertValues(void);

/**
 * @brief Calculate yaw angle
 * 
 * @return float Calculated yaw angle
 */
float calcAngleX(void);

/**
 * @brief Calculate pitch angle
 * 
 * @return float Calculated pitch angle
 */
float calcAngleY(void);

#endif /* MPU6050_CONTROL_H_ */
