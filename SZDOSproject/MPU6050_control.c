/*
 * MPU6050_control.c
 *
 *  Created on: Jan 29, 2020
 *      Author: damja
 */
#include <math.h>
#include <stdint.h>
#include "i2c_functions.h"

#include "MPU6050_control.h"

float AccX, AccY, AccZ;
float GyroX, GyroY, GyroZ;

uint8_t initMPU6050(void)
{
    InitI2C0();

    if(I2CReceive(MPU6050_I2C_ADDRESS, WHO_AM_I) != 'h'){
        //Error occured
        return 1;
    }

    I2CSend(MPU6050_I2C_ADDRESS, 2, PWR_MGMT_1, 0x00);
    I2CSend(MPU6050_I2C_ADDRESS, 2, INT_PIN_CFG, 0xD0);
    I2CSend(MPU6050_I2C_ADDRESS, 2, INT_ENABLE, 0x01);
    I2CSend(MPU6050_I2C_ADDRESS, 2, CONFIG, 0x01);
    I2CSend(MPU6050_I2C_ADDRESS, 2, SMPRT_DIV, 99);
    I2CSend(MPU6050_I2C_ADDRESS, 2, GYRO_CONFIG, 0x00);
    I2CSend(MPU6050_I2C_ADDRESS, 2, ACCEL_CONFIG, 0x00);

    return 0;
}

static int16_t data[6];

void convertValues(void)
{
    AccX = 0.0005985482f * data[0];
    AccY = 0.0005985482f * data[1];
    AccZ = 0.0005985482f * data[2];

    GyroX = 1.3323124e-4 * data[3];
    GyroY = 1.3323124e-4 * data[4];
    GyroZ = 1.3323124e-4 * data[5];
}

void getValues(void)
{
    I2CReceive6Bytes(MPU6050_I2C_ADDRESS, ACCEL_XOUT_H, data);
    I2CReceive6Bytes(MPU6050_I2C_ADDRESS, GYRO_XOUT_H, &(data[3]));

//    data[0] = (I2CReceive(MPU6050_I2C_ADDRESS, ACCEL_XOUT_H) << 8) | I2CReceive(MPU6050_I2C_ADDRESS, ACCEL_XOUT_L);
//    data[1] = (I2CReceive(MPU6050_I2C_ADDRESS, ACCEL_YOUT_H) << 8) | I2CReceive(MPU6050_I2C_ADDRESS, ACCEL_YOUT_L);
//    data[2] = (I2CReceive(MPU6050_I2C_ADDRESS, ACCEL_ZOUT_H) << 8) | I2CReceive(MPU6050_I2C_ADDRESS, ACCEL_ZOUT_L);
//
//    data[3] = (I2CReceive(MPU6050_I2C_ADDRESS, GYRO_XOUT_H) << 8) | I2CReceive(MPU6050_I2C_ADDRESS, GYRO_XOUT_L);
//    data[4] = (I2CReceive(MPU6050_I2C_ADDRESS, GYRO_YOUT_H) << 8) | I2CReceive(MPU6050_I2C_ADDRESS, GYRO_YOUT_L);
//    data[5] = (I2CReceive(MPU6050_I2C_ADDRESS, GYRO_ZOUT_H) << 8) | I2CReceive(MPU6050_I2C_ADDRESS, GYRO_ZOUT_L);

    convertValues();

//    int8_t data[14];
//    I2CBunchReceive(MPU6050_I2C_ADDRESS, ACCEL_XOUT_H, 6, data);
//    AccX = 0.0005985482f * ((data[0] << 8) | data[1]);
//    AccY = 0.0005985482f * ((data[2] << 8) | data[3]);
//    AccZ = 0.0005985482f * ((data[4] << 8) | data[5]);
//
//    I2CBunchReceive(MPU6050_I2C_ADDRESS, GYRO_XOUT_H, 6, data);
//    GyroX = 1.3323124e-4 * ((data[0] << 8) | data[1]);
//    GyroY = 1.3323124e-4 * ((data[2] << 8) | data[3]);
//    GyroZ = 1.3323124e-4 * ((data[4] << 8) | data[5]);
    return;
}

float calcAngleX(void)
{
    return atan2(AccX, sqrt(AccZ * AccZ + AccY * AccY)) * 180/3.14159265;
}

float calcAngleY(void)
{
    return atan2(AccY, sqrt(AccZ * AccZ + AccX * AccX)) * 180/3.14159265;
}
