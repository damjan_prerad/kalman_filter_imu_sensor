
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"

#include "utils/uartstdio.h"
#include "driverlib/rom.h"

#include <stdarg.h>
#include "inc/hw_i2c.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "driverlib/i2c.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"

#include "inc/hw_ints.h"
#include "driverlib/interrupt.h"

#include "i2c_functions.h"
#include "MPU6050_control.h"
#include "UART_specific.h"
#include "kalman_calculations.h"

volatile bool interruptFlag = false;

/*
 * System state vector
 * Xx[0] - Yaw angle
 * Xx[1] - Yaw gyroscope drift
 */
float Xx[2] = { 0.0f };

/*
 * Covariance error matrix for Yaw state
 */
float Px[2][2] = { 0.0f };

/*
 * System state vector
 * Xx[0] - Pitch angle
 * Xx[1] - Pitch gyroscope drift
 */
float Xy[2] = { 0.0f };

/*
 * Covariance error matrix for Pitch state
 */
float Py[2][2] = { 0.0F };


float Q_theta = 0.004f;
float Q_omega = 0.004f;
float R = 0.005f;
float dt = 0.1;

void GPIOAIntHandler(void) {
    GPIOIntClear(GPIO_PORTF_BASE, GPIO_INT_PIN_4);
    interruptFlag = true;
}


int main(void)
{
    ROM_SysCtlClockSet(SYSCTL_SYSDIV_5 | SYSCTL_USE_PLL | SYSCTL_XTAL_16MHZ |
                       SYSCTL_OSC_MAIN);

    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);

    //----------------------------------------------------------
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
    SysCtlDelay(3);

    GPIOPinTypeGPIOInput(GPIO_PORTF_BASE, GPIO_PIN_4);
    GPIOPadConfigSet(GPIO_PORTF_BASE,GPIO_PIN_4,GPIO_STRENGTH_2MA,GPIO_PIN_TYPE_STD_WPU);

    GPIOIntTypeSet(GPIO_PORTF_BASE,GPIO_PIN_4,GPIO_FALLING_EDGE);
    GPIOIntRegister(GPIO_PORTF_BASE,GPIOAIntHandler);
    GPIOIntEnable(GPIO_PORTF_BASE, GPIO_INT_PIN_4);
    //----------------------------------------------------------

    ConfigureUART();
    initMPU6050();

    UARTprintf("Everything's ready");

    float angleX;
    float angleY;

    while(1)
    {
        if(interruptFlag)
        {
        getValues();

        int i = 0;
        i++;

        angleX = calcAngleX();
        angleY = calcAngleY();

        applyKalmanFiltering(angleX, GyroX, Xx, Px);
        applyKalmanFiltering(angleY, GyroY, Xy, Py);

        //printEverything(AccX, AccY, AccZ, GyroX, GyroY, GyroZ, Xx[0], angleX, Xy[0], angleY);
        printAngles(Xx[0], angleX, Xy[0], angleY);
        //printRawValues(AccX, AccY, AccZ, GyroX, GyroY, GyroZ);

        interruptFlag = false;
        }

        /*
         * Save power by sending uC to sleep
         */
        ROM_SysCtlSleep();
        //SysCtlDelay(100 * (SysCtlClockGet() / 3 / 1000));
    }

}
