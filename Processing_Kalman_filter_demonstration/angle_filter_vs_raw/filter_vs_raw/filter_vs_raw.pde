
import processing.serial.*;

PFont f;

Serial dataInput;

int spacing = 120;

float[] filtered_angleXdata;
float[] raw_angleXdata;
float[] filtered_angleYdata;
float[] raw_angleYdata;

float filtered_angleX;
float raw_angleX;

float filtered_angleY;
float raw_angleY;

void setup(){
  dataInput = new Serial(this, Serial.list()[1], 115200);
  
  size(900, 900);
  f = createFont("Arial", 16, true);
  textFont(f, 36);
  
  filtered_angleXdata = new float [width];
  raw_angleXdata = new float [width];
  
  filtered_angleYdata = new float [width];
  raw_angleYdata = new float [width];
}

void update_filtered_angleXdata(float filtered_angleX){
  stroke(255,0,0);
  
  fill(0);
  text("Filtrirani ugao X:", 400, 2 * spacing - 100);
  text(str(filtered_angleX),700, 2 * spacing - 100);
  
  for(int i = filtered_angleXdata.length - 1; i > 0; i--){
    filtered_angleXdata[i] = filtered_angleXdata[i - 1];
  }
  filtered_angleXdata[0] = 3 * filtered_angleX;

  for(int i = 1; i < filtered_angleXdata.length; i++){
    line(i, filtered_angleXdata[i] + 2 * spacing, i - 1, filtered_angleXdata[i - 1] + 2 * spacing);  
  }
}

void update_raw_angleXdata(float raw_angleX){
  stroke(0,0,255);
  
  fill(0);
  text("Direktni ugao X:", 400, 2 * spacing - 150);
  text(str(raw_angleX),700, 2 * spacing - 150);
  
  for(int i = raw_angleXdata.length - 1; i > 0; i--){
    raw_angleXdata[i] = raw_angleXdata[i - 1];
  }
  raw_angleXdata[0] = 3 * raw_angleX;
  
  for(int i = 1; i < raw_angleXdata.length; i++){
    line(i, raw_angleXdata[i] + 2 * spacing, i - 1, raw_angleXdata[i - 1] + 2 * spacing);  
  }
}

void update_filtered_angleYdata(float filtered_angleY){
  stroke(255,0,0);
  
  fill(0);
  text("Filtrirani ugao Y:", 400, 5 * spacing - 100);
  text(str(filtered_angleY),700, 5 * spacing - 100);
  
  for(int i = filtered_angleYdata.length - 1; i > 0; i--){
    filtered_angleYdata[i] = filtered_angleYdata[i - 1];
  }
  filtered_angleYdata[0] = 3 * filtered_angleY;

  for(int i = 1; i < filtered_angleYdata.length; i++){
    line(i, filtered_angleYdata[i] + 5 * spacing, i - 1, filtered_angleYdata[i - 1] + 5 * spacing);  
  }
}

void update_raw_angleYdata(float raw_angleY){
  stroke(0,0,255);
  
  fill(0);
  text("Direktni ugao Y:", 400, 5 * spacing - 150);
  text(str(raw_angleY),700, 5 * spacing - 150);
  
  for(int i = raw_angleYdata.length - 1; i > 0; i--){
    raw_angleYdata[i] = raw_angleYdata[i - 1];
  }
  raw_angleYdata[0] = 3 * raw_angleY;
  
  for(int i = 1; i < raw_angleXdata.length; i++){
    line(i, raw_angleYdata[i] + 5 * spacing, i - 1, raw_angleYdata[i - 1] + 5 * spacing);  
  }
}

void draw(){
  while (dataInput.available() > 0) {
    String inBuffer = dataInput.readString();   
    if (inBuffer != null) {
      //println(inBuffer);
      String singleData [] = split(inBuffer, '#');
      if(singleData.length == 4){
          filtered_angleX = float(trim(singleData[0]));
          raw_angleX = float(trim(singleData[1]));
          filtered_angleY = float(trim(singleData[2]));
          raw_angleY = float(trim(singleData[3]));
          
          print(filtered_angleX + "      ");
          print(raw_angleX + "      ");
          print(filtered_angleY + "      ");
          println(raw_angleY);
          
          background(204);
          
          update_raw_angleXdata(raw_angleX);
          update_filtered_angleXdata(filtered_angleX);
           
          update_raw_angleYdata(raw_angleY);
          update_filtered_angleYdata(filtered_angleY);
      }
    }
  }
}
