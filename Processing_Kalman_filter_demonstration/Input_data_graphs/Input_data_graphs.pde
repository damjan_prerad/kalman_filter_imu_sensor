
import processing.serial.*;

Serial dataInput;

int spacing = 120;

float[] AccXdata;
float[] AccYdata;
float[] AccZdata;

float[] GyroXdata;
float[] GyroYdata;
float[] GyroZdata;

void setup(){
  dataInput = new Serial(this, Serial.list()[1], 115200);
  
  size(900, 900);
  AccXdata = new float [width];
  AccYdata = new float [width];
  AccZdata = new float [width];
  
  GyroXdata = new float [width];
  GyroYdata = new float [width];
  GyroZdata = new float [width];
}


float AccX;
float AccY;
float AccZ;

float GyroX;
float GyroY;
float GyroZ;

void updateAccXDrawing(float AccX){
  for(int i = AccXdata.length - 1; i > 0; i--){
    AccXdata[i] = AccXdata[i - 1];
  }
  AccXdata[0] = 5 * AccX;
  stroke(0,0,255);
  for(int i = 1; i < AccXdata.length; i++){
    line(i, AccXdata[i] + 4*spacing, i - 1, AccXdata[i - 1] + 4*spacing);  
  }
}

void updateAccYDrawing(float AccY){
  for(int i = AccYdata.length - 1; i > 0; i--){
    AccYdata[i] = AccYdata[i - 1];
  }
  AccYdata[0] = 5 * AccY;
  
  for(int i = 1; i < AccYdata.length; i++){
    line(i, AccYdata[i] + 2 * spacing, i - 1, AccYdata[i - 1] + 2 * spacing);  
  }
}

void updateAccZDrawing(float AccZ){
  for(int i = AccZdata.length - 1; i > 0; i--){
    AccZdata[i] = AccZdata[i - 1];
  }
  AccZdata[0] = 5 * AccZ;
 
  for(int i = 1; i < AccZdata.length; i++){
    line(i, AccZdata[i] + 3 * spacing, i - 1, AccZdata[i - 1] + 3 * spacing);  
  }
}

void updateGyroXDrawing(float GyroX){
  for(int i = GyroXdata.length - 1; i > 0; i--){
    GyroXdata[i] = GyroXdata[i - 1];
  }
  GyroXdata[0] = 5 * GyroX;
  
  for(int i = 1; i < GyroXdata.length; i++){
    line(i, GyroXdata[i] + 4 * spacing, i - 1, GyroXdata[i - 1] + 4 * spacing);  
  }
}

void updateGyroYDrawing(float GyroY){
  for(int i = GyroYdata.length - 1; i > 0; i--){
    GyroYdata[i] = GyroYdata[i - 1];
  }
  GyroYdata[0] = 5 * GyroY;
  
  for(int i = 1; i < GyroYdata.length; i++){
    line(i, GyroYdata[i] + 5 * spacing, i - 1, GyroYdata[i - 1] + 5 * spacing);  
  }
}

void updateGyroZDrawing(float GyroZ){
  for(int i = GyroZdata.length - 1; i > 0; i--){
    GyroZdata[i] = GyroZdata[i - 1];
  }
  GyroZdata[0] = 5 * GyroZ;
  stroke(255,0,0);
  for(int i = 1; i < GyroZdata.length; i++){
    line(i, GyroZdata[i] + 4 * spacing, i - 1, GyroZdata[i - 1] + 4 * spacing);  
  }
}

void draw(){
  while (dataInput.available() > 0) {
    String inBuffer = dataInput.readString();   
    if (inBuffer != null) {
      //println(inBuffer);
      String singleData [] = split(inBuffer, '#');
      if(singleData.length == 6){
          AccX = float(trim(singleData[0]));
          AccY = float(trim(singleData[1]));
          AccZ = float(trim(singleData[2]));
          
          GyroX = float(trim(singleData[3]));
          GyroY = float(trim(singleData[4]));
          GyroZ = float(trim(singleData[5]));
          
          print(AccX + "      ");
          print(AccY + "      ");
          print(AccZ + "      ");
          print(GyroX + "      ");
          print(GyroY + "      ");
          println(GyroZ);
          
          background(204);
          updateAccXDrawing(AccX);
          //updateAccYDrawing(AccY);
          //updateAccZDrawing(AccZ);
          
          //updateGyroXDrawing(GyroX);
          //updateGyroYDrawing(GyroY);
          updateGyroZDrawing(GyroZ);
      }
    }
  }
}
