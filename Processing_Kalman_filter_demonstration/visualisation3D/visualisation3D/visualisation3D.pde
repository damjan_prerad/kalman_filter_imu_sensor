
import processing.serial.*;

Serial dataInput;

float filtered_angleX;
float raw_angleX;

float filtered_angleY;
float raw_angleY;

void setup(){
  dataInput = new Serial(this, Serial.list()[1], 115200);
  
  size(900, 900, P3D);
}

void draw(){
  while (dataInput.available() > 0) {
    String inBuffer = dataInput.readString();   
    if (inBuffer != null) {
      //println(inBuffer);
      String singleData [] = split(inBuffer, '#');
      if(singleData.length == 4){
          filtered_angleX = float(trim(singleData[0]));
          raw_angleX = float(trim(singleData[1]));
          filtered_angleY = float(trim(singleData[2]));
          raw_angleY = float(trim(singleData[3]));
          
          lights();
          background(128, 64, 32);
          
          translate(width/2, height/2, 0);
          
          pushMatrix();
          translate(100, 0, 0);
          rotateX(filtered_angleX*PI/180);
          rotateZ(filtered_angleY*PI/180);
          stroke(255, 255, 255);
          fill(196,128,64);
          box(100);
          popMatrix();
          
          pushMatrix();
          translate(-100, 0, 0); 
          rotateX(raw_angleX*PI/180);
          rotateZ(raw_angleY*PI/180);
          stroke(255, 255, 255);
          fill(196,128,64);
          box(100);
          popMatrix();
      }
    }
  }
}
