
import processing.serial.*;

PFont f;

Serial dataInput;

int spacing = 120;

int[] data_array;

float accX;
float accY;
float accZ;

float gyroX;
float gyroY;
float gyroZ;

float filtered_angleX;
float raw_angleX;

float filtered_angleY;
float raw_angleY;

void setup(){
  dataInput = new Serial(this, Serial.list()[1], 115200);
  
  size(900, 900);
  f = createFont("Arial", 16, true);
  textFont(f, 36);
  
  data_array = new int [width];

}

void update_graph(float data){
  stroke(255,0,0);
  
  fill(0);
  text("Histogram", 400, 2 * spacing - 100);
  
  data_array[int(100 * data)] --;

  for(int i = 1; i < data_array.length; i++){
    line(i, data_array[i] + 2 * spacing, i - 1, data_array[i - 1] + 2 * spacing);  
  }
}

int state = 0;

void checkInput(){
  if(keyPressed == true){
   if(key == 'q') state = 0;
   if(key == 'w') state = 1;
   if(key == 'e') state = 2;
   if(key == 'r') state = 3;
   if(key == 't') state = 4;
   if(key == 'y') state = 5;
   if(key == 'u') state = 6;
   if(key == 'i') state = 7;
   if(key == 'o') state = 8;
   if(key == 'p') state = 9;
  }
}

void draw(){
  while (dataInput.available() > 0) {
    String inBuffer = dataInput.readString();   
    if (inBuffer != null) {
      //println(inBuffer);
      String singleData [] = split(inBuffer, '#');
      if(singleData.length == 10){
          checkInput();
          
          accX = float(trim(singleData[0]));
          accY = float(trim(singleData[1]));
          accZ = float(trim(singleData[2]));
          
          gyroX = float(trim(singleData[3]));
          gyroY = float(trim(singleData[4]));
          gyroZ = float(trim(singleData[5]));
        
          filtered_angleX = float(trim(singleData[6]));
          raw_angleX = float(trim(singleData[7]));
          filtered_angleY = float(trim(singleData[8]));
          raw_angleY = float(trim(singleData[9]));
          
          print(accX + "      ");
          print(accY + "      ");
          print(accZ + "      ");
          print(gyroX + "      ");
          print(gyroY + "      ");
          print(gyroZ + "      ");
          print(filtered_angleX + "      ");
          print(raw_angleX + "      ");
          print(filtered_angleY + "      ");
          println(raw_angleY);
          
          background(204);
          
          update_graph(raw_angleX);
      }
    }
  }
}
